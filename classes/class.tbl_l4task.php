<?php

global $dbpath;
require_once $dbpath;

class l4task {
    private $conn;
    private $tblname;


    // Constructor
    public function __construct(){
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
        $this->tblname="tbl_l4task";

    }


    // Execute queries SQL
    public function runQuery($sql){
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    // Insert
    public function insert($name, $email){
        try{
            $stmt = $this->conn->prepare("INSERT INTO ".$this->tblname." (l4taskname, email) VALUES(:name, :email)");
            $stmt->bindparam(":name", $name);
            $stmt->bindparam(":email", $email);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Update
    public function update($name, $email, $id){
        try{
            echo "testupdate";
            $stmt = $this->conn->prepare("UPDATE ".$this->tblname." SET l4taskname = :name, email = :email WHERE l4taskid = :id");
            $stmt->bindparam(":name", $name);
            $stmt->bindparam(":email", $email);
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Delete
    public function delete($id){
        try{
            $stmt = $this->conn->prepare("DELETE FROM ".$this->tblname." WHERE l4taskid = :id");
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    // Redirect URL method
    public function redirect($url){
        header("Location: $url");
    }
}
?>
