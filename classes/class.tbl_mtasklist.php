<?php

global $dbpath;
require_once $dbpath;

class mtasklist {
    private $conn;
    private $tblname;


    // Constructor
    public function __construct(){
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
        $this->tblname="tbl_mtasklist";

    }


    // Execute queries SQL
    public function runQuery($sql){
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    // Insert
    public function insert($name){
        try{
            $stmt = $this->conn->prepare("INSERT INTO ".$this->tblname." (TaskListDescription) VALUES(:name)");
            $stmt->bindparam(":name", $name);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Update
    public function update($name, $id){
        try{
           // echo "testupdate";
            $stmt = $this->conn->prepare("UPDATE ".$this->tblname." SET TaskListDescription = :name  WHERE TaskListID= :id");
            $stmt->bindparam(":name", $name);
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Delete
    public function delete($id){
        try{
            $stmt = $this->conn->prepare("DELETE FROM ".$this->tblname." WHERE TaskListID = :id");
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    // Redirect URL method
    public function redirect($url){
        header("Location: $url");
    }
}
?>
