<?php

function tbl_l5taskitem_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="l5taskitem Management";
    $objType="l5taskitem";
    $tblname="tbl_l5taskitem";
    $idcol="l5taskitemid";
    require_once 'classes/class.tbl_l5taskitem.php';
    $objl5taskitem = new l5taskitem();
    processl5taskitem($objl5taskitem,$menu);

    echo "<h1 style=\"margin-top: 10px\">$subpagetitle</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }


    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listl5taskitem':

                listl5taskitem($objl5taskitem,$tblname,$idcol,$menu);
                break;

            case 'addl5taskitem':
                addl5taskitem($objl5taskitem,$tblname,$idcol,$menu);
                break;
            case 'editl5taskitem':
                editl5taskitem($objl5taskitem,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listl5taskitem($objl5taskitem,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listl5taskitem($objl5taskitem,$tblname,$idcol,$menu);
    }

}
function processl5taskitem($objl5taskitem,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objl5taskitem->delete($id)){
                    $objl5taskitem->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['l5taskitemname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $name   = strip_tags($_POST['l5taskitemname']);
        $email  = strip_tags($_POST['email']);

        try{
            if($id !== ""){

                if($objl5taskitem->update($name, $email, $id)){

                    $objl5taskitem->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objl5taskitem->insert($name, $email)){
                    $objl5taskitem->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objl5taskitem->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listl5taskitem($objl5taskitem,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
            <?php
            $query = "SELECT * FROM $tblname";
            $stmt = $objl5taskitem->runQuery($query);
            $stmt->execute();
            ?>
            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowl5taskitem = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowl5taskitem[$idcol]);    ?>
                        </td>

                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editl5taskitem&edit_id=<?php print($rowl5taskitem[$idcol]);?>">
                                <?php print($rowl5taskitem['l5taskitemname']); ?></a>
                        </td>

                        <td><?php print($rowl5taskitem['email']);  ?></td>

                        <td>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowl5taskitem[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function addl5taskitem($objl5taskitem,$tblname,$idcol,$menu)
{
    echo "test";
    l5taskitemform($objl5taskitem,$tblname,$idcol,$menu);
}
function editl5taskitem($objl5taskitem,$tblname,$idcol,$menu)
{
    //echo "test";
    l5taskitemform($objl5taskitem,$tblname,$idcol,$menu);
}

function l5taskitemform($objl5taskitem,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objl5taskitem->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowl5taskitem = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowl5taskitem = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowl5taskitem[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="l5taskitemname" id="l5taskitemname" placeholder="First Name and Last Name" value="<?php print($rowl5taskitem['l5taskitemname']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Email *</label>
            <input  class="form-control" type="text" name="email" id="email" placeholder="johndoel@gmail.com" value="<?php print($rowl5taskitem['email']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}