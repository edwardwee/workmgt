<?php

function tbl_mtasklistitem_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="mtasklistitem Management";
    $objType="mtasklistitem";
    $tblname="tbl_mtasklistitem";
    $idcol="mtasklistitemid";
    require_once 'classes/class.tbl_mtasklistitem.php';
    $objmtasklistitem = new mtasklistitem();
    processmtasklistitem($objmtasklistitem,$menu);

    echo "<h1 style=\"margin-top: 10px\">$subpagetitle</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }


    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listmtasklistitem':

                listmtasklistitem($objmtasklistitem,$tblname,$idcol,$menu);
                break;

            case 'addmtasklistitem':
                addmtasklistitem($objmtasklistitem,$tblname,$idcol,$menu);
                break;
            case 'editmtasklistitem':
                editmtasklistitem($objmtasklistitem,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listmtasklistitem($objmtasklistitem,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listmtasklistitem($objmtasklistitem,$tblname,$idcol,$menu);
    }

}
function processmtasklistitem($objmtasklistitem,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objmtasklistitem->delete($id)){
                    $objmtasklistitem->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['mtasklistitemname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $name   = strip_tags($_POST['TaskListDescription']);
        $email  = strip_tags($_POST['email']);

        try{
            if($id !== ""){

                if($objmtasklistitem->update($name, $email, $id)){

                    $objmtasklistitem->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objmtasklistitem->insert($name, $email)){
                    $objmtasklistitem->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objmtasklistitem->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listmtasklistitem($objmtasklistitem,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
            <?php

            $query = "SELECT * FROM $tblname";
            $stmt = $objmtasklistitem->runQuery($query);
            $stmt->execute();
            ?>
            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowmtasklistitem = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowmtasklistitem[$idcol]);    ?>
                        </td>

                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editmtasklistitem&edit_id=<?php print($rowmtasklistitem[$idcol]);?>">
                                <?php print($rowmtasklistitem['mtasklistitemname']); ?></a>
                        </td>

                        <td><?php print($rowmtasklistitem['email']);  ?></td>

                        <td>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowmtasklistitem[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function listmtasklistitembytasklist($tasklistid)
{
    //echo "test";
    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Description</th>
                <th>Ops Line</th>
                <th>Ops Description</th>
                <th>Work Center</th>                <th>Dur. UOM</th>

                <th>Work</th>
                <th>Work UOM</th>
                <th>Duration</th>
                <th>Pax</th>
                <th></th>
            </tr>
            </thead>
            <?php
            require_once 'classes/class.tbl_mtasklistitem.php';
            $objmtasklistitem = new mtasklistitem();
            $idcol="TaskListItemID";
            $menu="mtasklistitem";
            $query = "SELECT * FROM tbl_mtasklistitem WHERE `TaskListID`=$tasklistid ORDER BY `TaskListID`,`TaskListItemDescription`,`OperationLine`";
            $stmt = $objmtasklistitem->runQuery($query);
            $stmt->execute();
            ?>
            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowmtasklistitem = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowmtasklistitem[$idcol]);    ?>
                        </td>

                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editmtasklistitem&edit_id=<?php print($rowmtasklistitem[$idcol]);?>">
                                <?php print($rowmtasklistitem['TaskListItemDescription']); ?></a>
                        </td>

                        <td><?php print($rowmtasklistitem['OperationLine']);  ?></td>
                        <td><?php print($rowmtasklistitem['OperationDescription']);  ?></td>
                        <td><?php print($rowmtasklistitem['Operation Work Center']);  ?></td>
                        <td><?php print($rowmtasklistitem['OperationNumberofWorkers']);  ?></td>
                        <td><?php print($rowmtasklistitem['Operation Work']);  ?></td>
                        <td><?php print($rowmtasklistitem['Work UOM']);  ?></td>
                        <td><?php print($rowmtasklistitem['Operation Duration']);  ?></td>
                        <td><?php print($rowmtasklistitem['Operation Duration UOM']);  ?></td>
                        <td>
                            <!--
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowmtasklistitem[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                            -->
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function addmtasklistitem($objmtasklistitem,$tblname,$idcol,$menu)
{
    echo "test";
    mtasklistitemform($objmtasklistitem,$tblname,$idcol,$menu);
}
function editmtasklistitem($objmtasklistitem,$tblname,$idcol,$menu)
{
    //echo "test";
    mtasklistitemform($objmtasklistitem,$tblname,$idcol,$menu);
}

function mtasklistitemform($objmtasklistitem,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objmtasklistitem->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowmtasklistitem = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowmtasklistitem = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowmtasklistitem[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="mtasklistitemname" id="mtasklistitemname" placeholder="First Name and Last Name" value="<?php print($rowmtasklistitem['mtasklistitemname']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Email *</label>
            <input  class="form-control" type="text" name="email" id="email" placeholder="johndoel@gmail.com" value="<?php print($rowmtasklistitem['email']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}