<?php

function tbl_account_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="account Management";
    $objType="account";
    $tblname="tbl_account";
    $idcol="accID";
    require_once 'classes/class.tbl_account.php';
    $objaccount = new account();
    processaccount($objaccount,$menu);

    echo "<h1 style=\"margin-top: 10px\">$subpagetitle</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }


    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listaccount':

                listaccount($objaccount,$tblname,$idcol,$menu);
                break;

            case 'addacc':
                echo "testacc entry";
                addaccount($objaccount,$tblname,$idcol,$menu);
                break;
            case 'editaccount':
                editaccount($objaccount,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listaccount($objaccount,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listaccount($objaccount,$tblname,$idcol,$menu);
    }

}
function processaccount($objaccount,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objaccount->delete($id)){
                    $objaccount->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['accountname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $name   = strip_tags($_POST['accountname']);
        $company= strip_tags($_POST['company']);
        $owner= strip_tags($_POST['owner']);
        $admin= strip_tags($_POST['admin']);

        try{
            if($id !== ""){

                if($objaccount->update($name,$company,$owner,$admin,  $id)){

                    $objaccount->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objaccount->insert($name,$company,$owner,$admin)){
                    $objaccount->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objaccount->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listaccount($objaccount,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Account Name</th>
                <th>Company</th>
                <th>Account Owner</th>
                <th>Account Admin</th>
                <th></th>
            </tr>
            </thead>
            <?php
            $query = "SELECT * FROM $tblname";
            $stmt = $objaccount->runQuery($query);
            $stmt->execute();
            ?>
            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowaccount = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowaccount[$idcol]);    ?>
                        </td>

                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editaccount&edit_id=<?php print($rowaccount[$idcol]);?>">
                                <?php print($rowaccount['accName']); ?></a>
                        </td>

                        <td><?php print($rowaccount['accCompany']);  ?></td>
                        <td><?php print($rowaccount['accOwner']);  ?></td>
                        <td><?php print($rowaccount['accAdmin']);  ?></td>

                        <td>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowaccount[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function addaccount($objaccount,$tblname,$idcol,$menu)
{
    echo "test";
    accountform($objaccount,$tblname,$idcol,$menu);
}
function editaccount($objaccount,$tblname,$idcol,$menu)
{
    //echo "test";
    accountform($objaccount,$tblname,$idcol,$menu);
}

function accountform($objaccount,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objaccount->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowaccount = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowaccount = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowaccount[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="accountname" id="accountname" placeholder="First Name and Last Name" value="<?php print($rowaccount['accName']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Company *</label>
            <input  class="form-control" type="text" name="company" id="company" placeholder="Company Name" value="<?php print($rowaccount['accCompany']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Account Owner *</label>
            <input  class="form-control" type="text" name="owner" id="owner" placeholder="Owner Name" value="<?php print($rowaccount['accOwner']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Account Admin *</label>
            <input  class="form-control" type="text" name="admin" id="admin" placeholder="Admin Name" value="<?php print($rowaccount['accAdmin']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}