<?php

function tbl_mtasklist_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="mtasklist Management";
    $objType="mtasklist";
    $tblname="tbl_mtasklist";
    $idcol="TaskListID";
    require_once 'classes/class.tbl_mtasklist.php';
    $objmtasklist = new mtasklist();
    processmtasklist($objmtasklist,$menu);

    echo "<h1 style=\"margin-top: 10px\">$subpagetitle</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }


    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listmtasklist':

                listmtasklist($objmtasklist,$tblname,$idcol,$menu);
                break;

            case 'addmtasklist':
                addmtasklist($objmtasklist,$tblname,$idcol,$menu);
                break;
            case 'editmtasklist':
                editmtasklist($objmtasklist,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listmtasklist($objmtasklist,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listmtasklist($objmtasklist,$tblname,$idcol,$menu);
    }

}
function processmtasklist($objmtasklist,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objmtasklist->delete($id)){
                    $objmtasklist->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['mtasklistname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $name   = strip_tags($_POST['mtasklistname']);


        try{
            if($id !== ""){

                if($objmtasklist->update($name, $id)){

                    $objmtasklist->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objmtasklist->insert($name)){
                    $objmtasklist->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objmtasklist->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listmtasklist($objmtasklist,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Task List Description</th>

                <th></th>
            </tr>
            </thead>
            <?php
            $query = "SELECT * FROM $tblname";
            //echo $query;
            $stmt = $objmtasklist->runQuery($query);
            $stmt->execute();
            ?>
            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowmtasklist = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowmtasklist[$idcol]);    ?>
                        </td>

                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editmtasklist&edit_id=<?php print($rowmtasklist[$idcol]);?>">
                                <?php print($rowmtasklist['TaskListDescription']); ?></a>
                        </td>



                        <td>

                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowmtasklist[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>

                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function addmtasklist($objmtasklist,$tblname,$idcol,$menu)
{
    echo "test";
    mtasklistform($objmtasklist,$tblname,$idcol,$menu);
}
function editmtasklist($objmtasklist,$tblname,$idcol,$menu)
{
    //echo "test";
    mtasklistform($objmtasklist,$tblname,$idcol,$menu);

    //go to the Sublist
    include_once("includes/tbl_mtasklistitem.php");
    $tasklistid=$_REQUEST['edit_id'];
    listmtasklistitembytasklist($tasklistid);
}

function mtasklistform($objmtasklist,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objmtasklist->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowmtasklist = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowmtasklist = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowmtasklist[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="mtasklistname" id="mtasklistname" placeholder="First Name and Last Name" value="<?php print($rowmtasklist['TaskListDescription']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}