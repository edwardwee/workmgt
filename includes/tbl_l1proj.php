<?php

function tbl_l1proj_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="Project Management";
    $objType="l1proj";
    $tblname="tbl_l1proj";
    $idcol="projID";
    require_once 'classes/class.tbl_l1proj.php';
    $objl1proj = new l1proj();
    processl1proj($objl1proj,$menu);

    echo "<h1 style=\"margin-top: 10px\">$subpagetitle</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }


    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listl1proj':

                listl1proj($objl1proj,$tblname,$idcol,$menu);
                break;
            case 'view1proj':

                view1proj($objl1proj,$tblname,$idcol,$menu);
                break;
            case 'addproj':
                addl1proj($objl1proj,$tblname,$idcol,$menu);
                break;
            case 'editl1proj':
                editl1proj($objl1proj,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listl1proj($objl1proj,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listl1proj($objl1proj,$tblname,$idcol,$menu);
    }

}
function processl1proj($objl1proj,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objl1proj->delete($id)){
                    $objl1proj->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['l1projname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $posteddata[]="";
        echo "<table>";
        foreach ($_POST as $key => $value) {
            $posteddata[$key]=$value;
            echo "<tr>";
            echo "<td>";
            echo $key;
            echo "</td>";
            echo "<td>";
            echo $posteddata[$key];
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";

        $accID= $posteddata['accID'];
        $projName= $posteddata['projName'];
        $projRef= $posteddata['projRef'];
        $projStatus= $posteddata['projStatus'];
        $projManager= $posteddata['projManager'];
        $projManagerTeam= $posteddata['projManagerTeam'];
        $projLocation= $posteddata['projLocation'];
        $projDetailLocation= $posteddata['projDetailLocation'];
        $projTargetStart= $posteddata['projTargetStart'];
        $projTargetEnd= $posteddata['projTargetEnd'];
        $projScheduleStart= $posteddata['projScheduleStart'];
        $projScheduleEnd= $posteddata['projScheduleEnd'];
        $projActEnd=$posteddata['projActEnd'];
        $projActStart=$posteddata['projActStart'];

        try{
            if($id !== ""){

                if($objl1proj->update($accID, $projName,$projRef,$projStatus,$projManager,$projManagerTeam,$projLocation,$projDetailLocation,$projTargetStart,$projTargetEnd,$projScheduleStart,$projScheduleEnd,$projActStart,$projActEnd, $id)){

                    $objl1proj->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objl1proj->insert($accID, $projName,$projRef,$projStatus,$projManager,$projManagerTeam,$projLocation,$projDetailLocation,$projTargetStart,$projTargetEnd,$projScheduleStart,$projScheduleEnd)){
                //    $objl1proj->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objl1proj->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listl1proj($objl1proj,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <?php
    $query = "SELECT * FROM $tblname";
    $stmt = $objl1proj->runQuery($query);
    $stmt->execute();

    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>accID</th>
                <th>Project Name</th>
                <th>Project Ref</th>
                <th>Project Status</th>
                <th>Project Manager</th>
                <th>Project Team</th>
                <th>Location</th>
                <!--
                <th>Detail Location</th>
                <th>Target Start</th>
                <th>Target End</th>
                <th>Schedule Start</th>
                <th>Schedule End</th>
                <th>Actual Start</th>
                <th>Actual End</th>
                -->
                <th></th>
            </tr>
            </thead>

            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowl1proj = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowl1proj[$idcol]);    ?>
                        </td>
                        <td>
                            <?php print($rowl1proj['accID']);    ?>
                        </td>
                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=view1proj&id=<?php print($rowl1proj[$idcol]);?>">
                                <?php print($rowl1proj['projName']); ?></a>
                        </td>

                        <td><?php print($rowl1proj['projRef']);  ?></td>
                        <td><?php print($rowl1proj['projStatus']);  ?></td>
                        <td><?php print($rowl1proj['projManager']);  ?></td>
                        <td><?php print($rowl1proj['projManagerTeam']);  ?></td>
                        <td><?php print($rowl1proj['projLocation']);  ?></td>
                        <!--
                        <td><?php print($rowl1proj['projDetailLocation']);  ?></td>
                        <td>
                            <?php
                            $ddate=date("Y-m-d",strtotime($rowl1proj['projTargetStart']));
                            if($ddate=='1970-01-01')
                            {  $ddate=''; }
                            echo $ddate;

                            ?>
                        </td>
                        <td>
                            <?php
                            $ddate=date("Y-m-d",strtotime($rowl1proj['projTargetEnd']));
                            if($ddate=='1970-01-01')
                            {  $ddate=''; }
                            echo $ddate;

                            ?>
                        </td>
                        <td>
                            <?php
                            $ddate=date("Y-m-d",strtotime($rowl1proj['projScheduleStart']));
                            if($ddate=='1970-01-01')
                            {  $ddate=''; }
                            echo $ddate;

                            ?>
                        </td>
                        <td>
                            <?php
                            $ddate=date("Y-m-d",strtotime($rowl1proj['projScheduleEnd']));
                            if($ddate=='1970-01-01')
                            {  $ddate=''; }
                            echo $ddate;

                            ?>
                        </td>
                        <td>
                            <?php
                            $ddate=date("Y-m-d",strtotime($rowl1proj['projActStart']));
                            if($ddate=='1970-01-01')
                            {  $ddate=''; }
                            echo $ddate;

                            ?>
                        </td>
                        <td>
                            <?php
                            $ddate=date("Y-m-d",strtotime($rowl1proj['projActEnd']));
                            if($ddate=='1970-01-01')
                            {  $ddate=''; }
                            echo $ddate;

                            ?>
                        </td>
                        -->


                        <td>

                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editl1proj&edit_id=<?php print($rowl1proj[$idcol]);?>">
                                <span data-feather="edit"></span></a>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowl1proj[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>

                            </a>
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function view1proj($objl1proj,$tblname,$idcol,$menu)
{
    $id=$_REQUEST['id'];
    $query = "SELECT * FROM $tblname WHERE $idcol=$id";
    $stmt = $objl1proj->runQuery($query);
    $stmt->execute();

    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>accID</th>
                <th>Project Name</th>
                <th>Project Ref</th>
                <th>Project Status</th>
                <th>Project Manager</th>
                <th>Project Team</th>
                <th>Location</th>

                <th></th>
            </tr>
            </thead>

            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowl1proj = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowl1proj[$idcol]);    ?>
                        </td>
                        <td>
                            <?php print($rowl1proj['accID']);    ?>
                        </td>
                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=view1proj&id=<?php print($rowl1proj[$idcol]);?>">
                                <?php print($rowl1proj['projName']); ?></a>
                        </td>

                        <td><?php print($rowl1proj['projRef']);  ?></td>
                        <td><?php print($rowl1proj['projStatus']);  ?></td>
                        <td><?php print($rowl1proj['projManager']);  ?></td>
                        <td><?php print($rowl1proj['projManagerTeam']);  ?></td>
                        <td><?php print($rowl1proj['projLocation']);  ?></td>
                        <td>

                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editl1proj&edit_id=<?php print($rowl1proj[$idcol]);?>">
                                <span data-feather="edit"></span></a>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowl1proj[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>

                            </a>
                        </td>
                    </tr>


            </tbody>
        </table>

        <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>

                <th>Detail Location</th>
                <th>Target Start</th>
                <th>Target End</th>
                <th>Schedule Start</th>
                <th>Schedule End</th>
                <th>Actual Start</th>
                <th>Actual End</th>

            </tr>
            </thead>

            <tbody>

                        <td><?php print($rowl1proj['projDetailLocation']);  ?></td>
                        <td>
                            <?php
                        $ddate=date("Y-m-d",strtotime($rowl1proj['projTargetStart']));
                        if($ddate=='1970-01-01')
                        {  $ddate=''; }
                        echo $ddate;

                        ?>
                        </td>
                        <td>
                            <?php
                        $ddate=date("Y-m-d",strtotime($rowl1proj['projTargetEnd']));
                        if($ddate=='1970-01-01')
                        {  $ddate=''; }
                        echo $ddate;

                        ?>
                        </td>
                        <td>
                            <?php
                        $ddate=date("Y-m-d",strtotime($rowl1proj['projScheduleStart']));
                        if($ddate=='1970-01-01')
                        {  $ddate=''; }
                        echo $ddate;

                        ?>
                        </td>
                        <td>
                            <?php
                        $ddate=date("Y-m-d",strtotime($rowl1proj['projScheduleEnd']));
                        if($ddate=='1970-01-01')
                        {  $ddate=''; }
                        echo $ddate;

                        ?>
                        </td>
                        <td>
                            <?php
                        $ddate=date("Y-m-d",strtotime($rowl1proj['projActStart']));
                        if($ddate=='1970-01-01')
                        {  $ddate=''; }
                        echo $ddate;

                        ?>
                        </td>
                        <td>
                            <?php
                        $ddate=date("Y-m-d",strtotime($rowl1proj['projActEnd']));
                        if($ddate=='1970-01-01')
                        {  $ddate=''; }
                        echo $ddate;

                        ?>
                        </td>
                        </tr>


            </tbody>
        </table>
        <br>
        <?php
        //LIST ACTIVITIES
                include("includes/tbl_l2activity.php");

                tbl_l2activity_mgt($id);
                }
            }
            ?>



    </div>
     <?php

}

function addl1proj($objl1proj,$tblname,$idcol,$menu)
{
    echo "test";
    l1projform($objl1proj,$tblname,$idcol,$menu);
}
function editl1proj($objl1proj,$tblname,$idcol,$menu)
{
    //echo "test";
    l1projform($objl1proj,$tblname,$idcol,$menu);
}

function l1projform($objl1proj,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objl1proj->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowl1proj = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowl1proj = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowl1proj[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">AccID *</label>
            <input  class="form-control" type="text" name="accID" id="accID" placeholder="account ID" value="<?php print($rowl1proj['accID']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="name">Project Name *</label>
            <input  class="form-control" type="text" name="projName" id="projName" placeholder="Project Name" value="<?php print($rowl1proj['projName']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Project Ref *</label>
            <input  class="form-control" type="text" name="projRef" id="projRef" placeholder="Project Reference" value="<?php print($rowl1proj['projRef']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Project Status *</label>
            <input  class="form-control" type="text" name="projStatus" id="projStatus" placeholder="Project Status" value="<?php print($rowl1proj['projStatus']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Project Manager *</label>
            <input  class="form-control" type="text" name="projManager" id="projManager" placeholder="projManager" value="<?php print($rowl1proj['projManager']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Project Team *</label>
            <input  class="form-control" type="text" name="projManagerTeam" id="projManagerTeam" placeholder="projManagerTeam" value="<?php print($rowl1proj['projManagerTeam']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Location *</label>
            <input  class="form-control" type="text" name="projLocation" id="projLocation" placeholder="projLocation" value="<?php print($rowl1proj['projLocation']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Detail Location *</label>
            <input  class="form-control" type="text" name="projDetailLocation" id="projDetailLocation" placeholder="projDetailLocation" value="<?php print($rowl1proj['projDetailLocation']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Target Start *</label>
            <?php $ddate=date("Y-m-d",strtotime($rowl1proj['projTargetStart']));
            if($ddate=='1970-01-01'){$ddate='';}
            ?>
            <input  class="form-control" type="date" name="projTargetStart" id="projTargetStart" placeholder="projTargetStart" value="<?php print($ddate); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Target End *</label>
            <?php $ddate=date("Y-m-d",strtotime($rowl1proj['projTargetEnd']));
            if($ddate=='1970-01-01'){$ddate='';}
            ?>
            <input  class="form-control" type="date" name="projTargetEnd" id="projTargetEnd" placeholder="projTargetEnd" value="<?php print($ddate); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Schedule Start *</label>
            <?php $ddate=date("Y-m-d",strtotime($rowl1proj['projScheduleStart']));
            if($ddate=='1970-01-01'){$ddate='';}
            ?>
            <input  class="form-control" type="date" name="projScheduleStart" id="projScheduleStart" placeholder="projScheduleStart" value="<?php print($ddate); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Schedule End *</label>
            <?php $ddate=date("Y-m-d",strtotime($rowl1proj['projScheduleEnd']));
            if($ddate=='1970-01-01'){$ddate='';}
            ?>
            <input  class="form-control" type="date" name="projScheduleEnd" id="projScheduleEnd" placeholder="projScheduleEnd" value="<?php print($ddate); ?>" required maxlength="100">
        </div>

        <?php if(isset($_REQUEST['edit_id']))
            {?>
                <div class="form-group">
                    <label for="email">Act End *</label>
                    <?php $ddate=date("Y-m-d",strtotime($rowl1proj['projActStart']));
                    if($ddate=='1970-01-01'){$ddate='';}
                    ?>
                    <input  class="form-control" type="date" name="projActStart" id="projActStart" placeholder="projActStart" value="<?php print($ddate); ?>" required maxlength="100">
                </div>
                <div class="form-group">
                    <?php $ddate=date("Y-m-d",strtotime($rowl1proj['projActEnd']));
                    if($ddate=='1970-01-01'){$ddate='';}
                    ?>
                    <label for="email">Act End *</label>
                    <input  class="form-control" type="date" name="projActEnd" id="projActEnd" placeholder="projActEnd" value="<?php print($ddate); ?>" required maxlength="100">
                </div>

            <?php
            }
            ?>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}